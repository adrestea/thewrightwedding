﻿<%@ Page Title="The Wright Wedding" Language="C#"  AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>The Wright Wedding</title>
    
    <link href="https://fonts.googleapis.com/css?family=Alice|Zilla+Slab:400,700" rel="stylesheet">
    <link rel="stylesheet" href="css/styles-merged.css">
    <link rel="stylesheet" href="css/style.min.css">
    <link href="img/favicon.ico" rel="icon" type="image/x-icon" />
    <script src="js/jquery-3.3.1.min.js"></script>
    <!--[if lt IE 9]>
      <script src="js/vendor/html5shiv.min.js"></script>
      <script src="js/vendor/respond.min.js"></script>
    <![endif]-->
    <script>
        $(document).ready(function () {
            $("#Button1").click(function (event) {
                var errorText = $("#error");
                var yourName = $("#YourName");
                var yourEmail = $("YourEmail");
                var answer = $("input[name='Answer']:checked").val();

                var hasError = false;

                if (yourName.val() == "")
                {
                    hasError = true;
                }

                if (yourEmail.val() == "")
                {
                    hasError = true;
                }

                if (!answer)
                {
                    hasError = true;
                }

                if (hasError)
                {
                    event.preventDefault();
                    errorText.show();
                }
            });
        });
    </script>
  </head>
  <body>
    
    <!-- Fixed navbar -->
     <!-- navbar-fixed-top -->
    <nav class="navbar navbar-default probootstrap-navbar">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.html" title="uiCookies:Wedding">Chris <em>&amp;</em> Nat</a>
        </div>

        <div id="navbar-collapse" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li class="active"><a href="#" data-nav-section="home">Home</a></li>
            <li><a href="#" data-nav-section="groom-bride">Groom &amp; Bride</a></li>
            <li><a href="#" data-nav-section="the-wedding-party">The Wedding Party</a></li>
            <li><a href="#" data-nav-section="when-where">When &amp; Where</a></li>
            <li><a href="#" data-nav-section="information">Information</a></li>
            <li><a href="#" data-nav-section="rsvp">RSVP</a></li>
            <li><a href="#" data-nav-section="gifts-and-donations">Gifts &amp; Donations</a></li>
          </ul>
        </div>
      </div>
    </nav>
    
    <section class="flexslider" data-section="home">
      <div class="container text-intro-wrap">
        <div class="row">
          <div class="col-md-12 text-center text-intro probootstrap-animate">
            <h1>Chris <em>&amp;</em> Nat</h1>
            <h2>Getting Married on April 27, 2019 &mdash; The Orange Tree House, Greyabbey</h2>
          </div>
        </div>
      </div>
      <ul class="slides">
        <li style="background-image: url(img/hero_bg_1.jpg)" data></li>
      </ul>
    </section>

    <section class="probootstrap-section probootstrap-section-colored probootstrap-wedding-countdown">
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center probootstrap-animate"><h4>The Wedding Begins In</h4></div>
          <div class="col-md-12 probootstrap-animate">
            <div class="date-countdown"></div>  
          </div>
        </div>
      </div>
    </section>

    <section class="probootstrap-section probootstrap-bg" data-section="groom-bride">
      <div class="container">
        <div class="row">
          <div class="col-md-12 probootstrap-groom-bride">
            
            <div class="probootstrap-groom probootstrap-animate">
              <figure>
                <img src="img/chris_photo.png" class="img-responsive">
              </figure>
              <span class="probootstrap-caption">The Groom</span>
              <h2 class="text-center">Christopher Wright</h2>
              <p>Chris is a quiet guy. He is a logical thinker who likes to take his time making sure he is making the right decision. He works hard, loves computers and likes to keep to himself... unless he see's a dog, he's all about those. He enjoys the simple things in life; sleeping, good food, playing games online with his friends and a good cup of tea. Born just <i>three days</i> before Natalie and raised in the same town, attending all of the same schools and even sharing the same classes at times, Chris has always known of Natalie. After she caught his eye in school one lunch time, he mustered up the courage to add her as a friend on Bebo, then MSN and even got her number from a mutual friend. Online he was much more talkative and after a couple of weeks of chatting he finally asked her out via MSN. Over the next few years together he slowly introduced her to the land of anime and manga, new bands to listen to and she helped him to come out of his shell. True to his cautious thinking nature, he waited almost 9.5 years to pop the question. Each step was planned carefully, every word was googled to ensure it was painted correctly. Yes, painted. Similar to MSN where he wrote <i>"so... do you want to go out?"</i>, on the evening of November 30th 2017, after giving her fake birthday presents, he blindfolded a livid Natalie and lead her to their bedroom in their home where he told her to remove the blindfold and look at the wall then at him, he had painted the words ''Hey Natalie, will you marry me? :]''. This time however, he spoke the words when she turned to him, on his knee. Luckily for him, she agreed even though she was so angry at her wall. Now, together with Natalie, Chris is looking forward to starting their lives together married and seeing what the future holds.</p>
            </div>

            <div class="probootstrap-divider probootstrap-animate"><span>&amp;</span></div>

            <div class="probootstrap-bride probootstrap-animate">
              <figure>
                <img src="img/natalie_photo.png" class="img-responsive">
              </figure>
              <span class="probootstrap-caption">The Bride</span>
              <h2 class="text-center">Natalie Neill</h2>
			        <p>Natalie, bubbly and chatty is the opposite of Chris. If she hadn't spoken to him on her way to class after lunch all those years ago, who knows if they would have ended up together? She loves spending time with her sisters children and eating good food, usually from Bubba Drews, and a good cup of tea. She loves the idea of many tattoos and would love blue hair, taking notions where she really wants to get a massive tattoo without much thought as to what it would be. Thankfully Chris' logical thinking has kept her grounded so far.  Her favourite country is Japan, somewhere they have been once before with friends, and the place she has wanted to return to ever since. Over their years together she has been taught patience, waiting for 9.5 years to upgrade from girlfriend to fiancée. Knowing Chris might take ages to decide on a year to marry, she booked a venue viewing just 10 days later where, suprisingly, Chris who had said it was to only view and not book anything, suggested they book on that day! Maybe her influence has rubbed off on Chris and together they chose their wedding date for just two days after their 11 year anniversary. Together with Chris, Natalie is looking forward to celebrating their wedding and seeing what the future holds for them... and eating cake... and loads of food on their honeymoon</p>
            </div>

          </div>
        </div>
      </div>
    </section>

    <div class="probootstrap-section probootstrap-section-colored" data-section="the-wedding-party">
      <div class="container">
        <div class="row mb40 text-center">
          <div class="col-md-12 probootstrap-animate"><h2 class="mt0">Who's Who</h2></div>
        </div>
        <div class="row">
          <div class="col-md-12 probootstrap-animate">
            <h3>The Bride &amp; Groom</h3>
            <p>The bride, Natalie Neill, youngest daughter of Philip Neill and the groom, Christopher Wright, son of Susan and Trevor Wright, need some people by their sides while they prepare of their big day, as all couples do. Joining the bridal party as bridesmaids, Natalie has chosen her two sisters and long time friend.</p>
            <h3>The Bridesmaid</h3>
            <p><b>Katie</b> is Natalie's oldest sister. During their younger days they would not always get along. Katie would hit Natalie as she was a quiet child who wouldn't hit back...for a while anyway. Natalie would play tricks on Katie. Other days they were playing schools and house happily with their other sister and friends. Although they often fought during their teenage years, everything changed when Katie became pregnant with her eldest son, Anton. She began to make Natalie lunch on the days Natalie's classes ended early and they would joke about. Natalie ran around all of the closest shops to satisfy Katie's pregnancy cravings and together they attempted to build a high chair and put together a pram, much to the amusement of others. Soon, Natalie began to visit Katie's house every day either before or after work. As the years have passed, Katie has since gifted Natalie with three more wonderful children to dote on and play with and, although they do not get to see each other as often any more, these two sisters text daily and give each other advice on all subjects. They look out for one another and their bond has only gotten stronger. There was no doubt or hesitation in Natalie's decision in asking Katie to be by her side as a bridesmaid on this special day to share it with her.</p>
            <p><b>Naomi</b> is Natalie's (6 minute older as she will tell you) twin sister.  These two sisters were very close when they were younger to the point of refusing to go to school if their underwear didn't match. At times, one twin felt the other ones pain for her and knew something was wrong and even as Naomi was in theater being given anesthetic before having her tonsils removed, it was Natalie, lying in a side room upstairs waiting for her turn, that was falling asleep instead.  As children Naomi liked pink whereas Natalie hated it and loved blue, Naomi loved to play with horses, Natalie with tractors and trolls but they could always be found playing together and had their own 'language'. At night, there were times where they pushed their beds together to be closer to one another or held hands across the space between their beds. As they grew up they developed their own identities and friendship groups, at one point attending different schools and have since become quite competitive. As fate would have it, not long after Natalie and Chris began to date, Naomi began a relationship with Peter, a guy who just happened to have a mutual friend with Chris and now their friendship circle is once again the same. While their identities and styles are different, there are times when they are still mistaken for one another and every now and then would still play the odd trick together, such as Naomi calling Chris for Natalie since she is driving and speaking to him, with him thinking she was Natalie as their voices are the same. Although at times they may not contact each other for a while, Naomi and Natalie would meet at the gym or for a late night trip to Asda and it is as if they had never been apart. Of course it was a given that Naomi would be chosen as Natalie's bridesmaid. </p>
            <p><b>Lisa</b>, is Natalie's longtime friend. This pair met on their first day in a new school when they were placed beside each other in class and have remained in the same class for the rest of their school years. Lisa has been there for Natalie when she needed advice or someone to listen to and picked her up when she was down. No matter where life has taken them on their different paths or how long they may not see each other for, they always pick up right where they have left off. They have spent many evenings at Bubba Drews and watching movies, sitting at home cosy with treats and the odd spa trip or weekend away. Having a friend who is there for you no matter what, encourages you and isn't afraid to tell you things as they are without sugar-coating it is important and Natalie has found that in Lisa. As teenagers, they shared their dreams of marriage, who they would pick as bridesmaids and children's names - Lisa's name was always at top of the list. Natalie didn't even have to ask Lisa to be her bridesmaid when she announced the engagement because they both just knew.</p>
            <h3>The Bestmen</h3>
            <p><b>Thomas</b> pls.</p>
            <p><b>Martin</b> plz.</p>
            <h3>Our Special Guests</h3>
            <p>~</p>
          </div>
        </div>
      </div>
    </div>

    <div id="map"></div>

    <div class="probootstrap-section probootstrap-section-colored" data-section="when-where">
      <div class="container">
        <div class="row mb40 text-center">
          <div class="col-md-12 probootstrap-animate"><h2 class="mt0">When &amp; Where</h2></div>
        </div>
        <div class="row">
          <div class="col-md-6 probootstrap-animate">  
            <p><img src="img/img_2.png" class="img-responsive img-rounded"></p>
            <h3>Wedding Ceremony</h3>
            <p>The wedding will take play in the Orange Tree House, Greyabbey. It is an 1800s old church converted into the venue in a small village facing Strangford lough. The wedding ceremony will begin at <b>2:00PM</b>. We ask that guests arrive from <b>1:30PM</b> to make sure they get parked and can relax before the wedding ceremony begins.</p>
          </div>
          <div class="col-md-6 probootstrap-animate">
            <p><img src="img/img_1.png" class="img-responsive img-rounded"></p>
            <h3>Wedding Reception</h3>
            <p>After the wedding ceremony guests will be escorted from the main building for photos to be taken. This is to allow the venue time to swap the room around for the wedding party.</p>
          </div>
        </div>
      </div>
    </div>
    
    <section class="probootstrap-section" data-section="information">
      <div class="container">
        <div class="row mb40 probootstrap-animate">
          <div class="col-md-12 text-center"><h2>Information</h2></div>
        </div>
          <p>The wedding will begin at <b>2:00PM</b> and will be conducted by Rev David Cooper. The order of service is as follows:</p>
          <ul>
            <li>Welcome</li>
            <li>A short thanksgiving</li>
            <li>The Lighting of the Two Candles</li>
            <li>A Reading by Lisa/Nicole</li>
            <li>"I Do's"</li>
            <li>The Exchange of the Rings</li>
            <li><i>The Kiss</i></li>
            <li>The Lighting of the One Candle</li>
            <li>Rev David Cooper Shares Some Thoughts and Offers Us a Blessing</li>
            <li>A Prayer for the Bride And Grooms Life Togeather</li>
            <li>The Final Blessing</li>
            <li>The Signing of the Register.</li>
          </ul>
          <div class="col-md-12 text-center"><h3>Accommodation</h3></div>
          <p><b>Rose Cottage, Lilac Cottage and Foxglove Cottage</b> – 3 beautiful little self catering cottages beside the Abbey in the village and within walking distance of Orange Tree House, antiques shops, coffee shops, restaurants etc. Contact Janet – roomswithaview@rocketmail.com Tel: 07980 690250 www.homeaway.co.uk/1130644</p>
          <p><b>Cunningburn Cottages</b> www.cunningburncottages.com 028 9181 2828. Three two bedroom farm cottages, very pretty location, ten minutes drive from venue.</p>
          <p><b>Barnwell Farm Cottages</b>www.barnwellfarmcottages.co.uk 028 4278 8488. 7 cottages of different sizes, on site games room, very pretty location, five minutes drive from venue. 38 Main Street Greyabbey – 2 bedroom cottage www.38MainStreet.com</p>
          <p><b>Bar Hall Barns</b> - www.holidaycottagesportaferry.co.uk 07834 16 3687. Recommended for visitors wanting to use Portaferry as a base for a short holiday, these cottages are in a lovely setting but slightly further from the venue – 20–30 minutes drive at the very tip of the peninsula. Combines well with larger parties who have guests staying in the Portaferry Hotel.</p>
          <p><b>Edenvale House</b> www.edenvalehouse.com 028 9181 4881 Ballycastle House www.ballycastlehouse.com 028 4278 8357. Shoreline at Mount Stewart – 07858 480823 www.shorelinemountstewart.com</p>
          <p><b>Trasnagh House</b> http://www.discovernorthernireland.com/Trasnagh-House-Greyabbey-Newtownards-P4532</p>
          <p><b>Fiddler’s Green</b> – Traditional Irish Pub in picturesque Portaferry (15 minute drive) with 8 double B&B rooms (transport to and from Orange Tree on the wedding day may be arranged with Fiddlers in advance) 028 4272 8393 www.fiddlersgreenportaferry.com </p>
          <p><b>Ardbrea House</b> www.ardbrae.co.uk 028 4277 2914</p>
          <p><b>Ballynester House</b> www.ballynesterhouse.com (0) 28 427 88386 Ballyrusley B&B www.ballyrusleybandb.com 028 4272 9057</p>
          <p><b>Strangford Bay</b> Lodge www.strangfordbaylodge.com (028) 4272 8350. Excellent facilities for disabled guests and those with accessibility issues.</p>
          <p><b>Paul Arthurs</b> Restaurant & Rooms www.arthurskircubbin.com 028 42738192Rooms attached to lively bistro and Irish bar in the nearby village of Kircubbin.</p>
          <p><b>Airbnb</b>, The following properties are marketed through Airbnb:
          <ul>
            <li>https://www.airbnb.co.uk/rooms/3513695</li>
            <li>https://www.airbnb.co.uk/rooms/3513695?s=1Km_</li>
            <li>https://www.airbnb.co.uk/rooms/6197139?s=x_7j</li>
          </ul></p>
          <p><b>Portaferry Hotel</b>, The Strand, Portaferry, Tel 028 4272 8231 </p>
          <p><b>Strangford Arms Hotel</b>, Newtownards 028 91814141 www.strangfordhotel.com </p>
          <p><b>Clandeboye Lodge Hotel</b>, Bangor 028 91852500 email - reception@clandeboyelodge.co.uk</p>
          <p><b>Marine Court Hotel</b>, Bangor 028 91451100 email – info@marinecourthotel.net</p>
          
          <!-- Taxi -->
          <div class="col-md-12 text-center"><h3>Taxis</h3></div>
          <p><b>Ards Cabs</b> – 02891811111</p>
          <p><b>Kare Kabs</b> – 02891818001</p>
          <p><b>Purple – based in Greyabbey</b> – 07922 582419</p>
          <p><b>Value Cabs</b> – 028 9080 9080</p>
          <p><b>Castlebawn Taxis</b> – 02891827222</p>
          <p><b>Coastal Cabs</b> – 02891888899</p>
          <p><b>Devoys Taxi</b> – 07950 131947</p>
          <p><b>Abbeydale</b> – 07836 361948</p>
          <p><b>DC Cabs</b> – 028 9145 9666</p>
          <p><b>Strangford Bay</b> Lodge www.strangfordbaylodge.com (028) 4272 8350. Excellent facilities for disabled guests and those with accessibility issues.</p>
          <p><b>Paul Arthurs</b> Restaurant & Rooms www.arthurskircubbin.com 028 42738192Rooms attached to lively bistro and Irish bar in the nearby village of Kircubbin.</p>
           
        </div>
    </section>

    <section class="probootstrap-section probootstrap-border-top" data-section="rsvp">
      <div class="container">
        <div class="row mb40">
          <div class="col-md-12 text-center">
            <h2 class="mt0 mb10 probootstrap-animate">Will you attend?</h2>
            <p>Please sign your RSVP</p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6 col-md-offset-3">
            <div class="probootstrap-card probootstrap-animate">
              <p>Alternatively please contact Chris (07707641209) or Nat (07756260782) to RSVP.</p>
              <form class="probootstrap-form" runat="server">
                <div class="form-group">
                  <label for="name">Your Name</label>
                  <asp:TextBox ID="YourName" runat="server" class="form-control" />
                </div>
                <div class="form-group">
                  <label for="email">Your Email</label>
                  <asp:TextBox ID="YourEmail" runat="server" class="form-control" />
                </div>
                <div class="form-group">
                  <p>Will you attend?</p>
                    <asp:RadioButtonList ID="Answer" runat="server">
                        <asp:ListItem Text=" Yes, I will be there" Value="yes" />
                        <asp:ListItem Text=" Sorry, I can't come" Value="no" />
                    </asp:RadioButtonList>
                </div>
                <div class="form-group">
                  <label for="note">Note</label>
                  <asp:TextBox ID="Note" runat="server" cols="20" rows="5" class="form-control" />
                </div>
                <div class="form-group">
                  <asp:Button ID="Button1" runat="server" class="btn btn-primary btn-lg" text="Send RSVP" OnClick="Submit_Email" />
                </div>
                <div id="error" style="display: none; color: red;">
                    <p>Please ensure you have entered a valid e-mail. And fill out every section (the note section is not mandatory).</p>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="probootstrap-section" data-section="gifts-and-donations">
      <div class="container">
        <div class="row mb40 probootstrap-animate">
          <div class="col-md-12 text-center"><h2>Gifts And Donations</h2></div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <p>Although you do not need to give a gift or donation, if you wish to give us something, any donations towards some of our intended activities are appreciated and we are thankful for. Please see GoFundMe website for more information on our plans for the honeymoon.</p>
                <embed height="600px" width="100%" src="https://www.gofundme.com/mvc.php?route=widgets/mediawidget&fund=the-wright039s-honeymoon-fund&image=1&coinfo=1&preview=1" type="text/html"></embed>
            </div>
        </div>
     </div>
    </section>

    <footer class="probootstrap-footer">
      <div class="container text-center">
        <div class="row">
          <div class="col-md-12">
              <p>The Wright Wedding</p>
              <p>glhf</p>
          </div>
        </div>
      </div>
    </footer>

    <script src="js/scripts.min.js"></script>
    <script src="js/custom.min.js"></script>

    <!-- Google Map -->
    <script>
      var map;
      function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: 54.533897, lng: -5.561247},
          zoom: 16
        });
      }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVNfhPM2Nm3A_h-U_symXZUqYmLmfkLDE&callback=initMap" async defer></script>
  </body>
</html>