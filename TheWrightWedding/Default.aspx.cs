﻿using System;
using System.Net.Mail;
using System.Web.UI;

public partial class _Default : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void Submit_Email(object sender, EventArgs e)
    {
        using (MailMessage message = new MailMessage())
        {
            message.From = new MailAddress("rsvp@thewrightwedding.info");
            message.To.Add(new MailAddress("chris.reflection@gmail.com"));
            message.To.Add(new MailAddress("natalie-neill@hotmail.co.uk"));
            message.Subject = "RSVP: " + YourName.Text.ToString();
            message.IsBodyHtml = true;
            message.Body = "<html><body><h3>From: " + YourEmail.Text.ToString() + "</h3><p>Attending wedding: " + Answer.SelectedItem.ToString() + "</p><p>Note: " + Note.Text.ToString() + "</p></body></html>";

            SmtpClient client = new SmtpClient();

            client.UseDefaultCredentials = false;
            client.EnableSsl = false;
            client.Send(message);
        }
    }
}