﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TheWrightWedding.Startup))]
namespace TheWrightWedding
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
